This module allows any user with appropriate permissions to create relationships between any two users.
The creation of the relationship starts from a user which is considered the requester.
The relationship is automatically approved by the requestee.
This module use a special AJAX widget "Dynamic Select" to select the requestee.
The list of requestees to select from is built from a view

Usage:
Create a view listing users with the display "Dynamic Select" with some non exposed filters. 
Configure the module admin/config/people/relationships/settings under the vertical tab "Management" by selecting the view created previously and the relevant filters.
Specify the labels of the HTML controls used by the widget and the display format.
Assign the appropriate role and permissions to the users you want to use that feature. 
Any users with appropriate permission can go to the edit page of any user and create relationships. 
Under Relationships tab, select the Management tab. The edited user is the requester.
Filter the list of users in the drop down if necessary with a keyword that will be passed on to the filters selected in the configuration, 
Pick up the requestee in the select box and create a new relationship.
Only the requestee(s) are listed in the table, unlike under the Relationship Current tab
The layout of the Dynamic Select widget can be changed by modifying the file user_relationships_manage.css
 
 

